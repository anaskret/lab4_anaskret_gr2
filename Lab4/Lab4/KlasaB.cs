﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class KlasaB : KlasaA
    {
        private int childId;
        private string address;

        public string Address
        {
            get
            {
                return address;
            }
            protected set
            {
                address = value;
            }
        }

        public int ChildId
        {
            get
            {
                return childId;
            }
        }

        public KlasaB(int id, int childId) : base(id)
        {
            this.childId = childId;
        }

        public string GetFullName()
        {
            return $"{FirstName} {LastName} {PhoneNumber}";
        }

        public string GetFullName(bool ifPhoneNumber)
        {
            if (ifPhoneNumber) return GetFullName();
            return $"{FirstName} {LastName}";
        }

        public override string ToString()
        {
            return $"ChildId: {ChildId}\nId: {Id}\nImie: {FirstName}\nNazwisko: {LastName}\nAdres: {Address}\nNr. Tel.: {PhoneNumber}";
        }

        public void SetFullNameAndAddress(string firstName, string lastName, string address)
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
        }
    }
}
