﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class KlasaA
    {
        private int id;
        private string firstName;
        private string lastName;

        public int Id
        {
            get
            {
                return id;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            protected set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            protected set
            {
                lastName = value;
            }
        }
        public string PhoneNumber { protected get; set; }

        public KlasaA(int id)
        {
            this.id = id;
        }

        public virtual string GetPhoneNumber()
        {
            return $"{PhoneNumber}";
        }

    }
}
