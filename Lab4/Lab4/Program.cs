﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;

            Console.WriteLine("Ile chcesz wpisac osob? ");
            int ilosc = Convert.ToInt32(Console.ReadLine());

            List<KlasaB> o = new List<KlasaB>();

            while (ilosc > i)
            { 
                Console.WriteLine("Podaj id: ");
                int id = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Podaj ChildId: ");
                int childId = Convert.ToInt32(Console.ReadLine());

                KlasaB osoba = new KlasaB(id, childId);

                Console.WriteLine("Podaj imie: ");
                string firstName = Console.ReadLine();

                Console.WriteLine("Podaj nazwisko: ");
                string lastName = Console.ReadLine();

                Console.WriteLine("Podaj adres (ulica): ");
                string address = Console.ReadLine();

                Console.WriteLine("Podaj numer telefonu: ");
                osoba.PhoneNumber = Console.ReadLine();

                osoba.SetFullNameAndAddress(firstName, lastName, address);

                
                o.Add(osoba);
                i++;
            }

            bool petla = true;

            while (petla)
            {
                Console.WriteLine("\nChcesz wyswietlic jednego z uzytkownikow?");
                Console.WriteLine("1. Tak\n2. Nie");
                int wybor = Convert.ToInt32(Console.ReadLine());
                switch(wybor)
                {
                    case 1:
                        Console.WriteLine("Wpisz id do wyszukania: ");

                        int searchId = Convert.ToInt32(Console.ReadLine());
                        int ileRazy = 0;

                        foreach (var item in o)
                        {
                            if (item.Id == searchId)
                            {
                                Console.WriteLine(item.ToString());
                                ileRazy++;

                            }
                        }

                        if (ileRazy == 0) Console.WriteLine("Nie znaleziono nikogo o takim id");

                        break;
                    case 2:
                        petla = false;
                        break;
                    default: Console.WriteLine("Wybrales zla opcje, sprobuj ponownie");
                        break;
                }
            }
        }
    }
}
